#!/usr/bin/env bash
set -e

echo "Installing kubernetes utilities..."
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list >/dev/null
sudo apt-get update
sudo apt-get install -y kubelet kubeadm kubectl
sudo apt-mark hold kubelet kubeadm kubectl

echo "Disabling swap..."
sudo swapoff -a
sudo sed -i '/ swap / s/^\(.*\)$/#\1/g' /etc/fstab

echo "Overriding node-ip for kubelet"
echo "KUBELET_EXTRA_ARGS=\"--node-ip=$(ip addr show eth1 | grep "inet\b" | awk '{print $2}' | cut -d/ -f1)\"" | sudo tee /etc/default/kubelet >/dev/null
