#!/usr/bin/env bash
set -e

echo "Installing docker-compose..."
sudo curl -Ls "https://github.com/docker/compose/releases/download/1.24.0/docker-compose-$(uname -s)-$(uname -m)" \
  -o /usr/local/bin/docker-compose
sudo curl -Ls https://raw.githubusercontent.com/docker/compose/1.24.0/contrib/completion/bash/docker-compose \
  -o /etc/bash_completion.d/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
