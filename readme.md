# IDM Kubernetes Cluster

This is a sample local kubernetes cluster, powered by Vagrant and VirtualBox, for learning
and testing purposes.

It consists of scripts that create and provision a Kubernetes cluster with:

- 1 master
- 2 nodes
- 1 external VM

The scripts configure the VMs with networking, install Kubernetes and the Dashboard, and
add a third-party HAProxy ingress controller. 


## Prerequisites

- VT-x enabled
- 16GB of RAM
- Vagrant 2.2.5
- VirtualBox 6
- Docker CE 18 + docker-compose


## Master (192.168.4.10)

Start the master VM, it will provision and configure itself automatically.

```sh
cd master
vagrant up
```


### Dashboard

To start the Dashboard, execute this on the master:

```sh
kubectl proxy --accept-hosts='.*' --address='0.0.0.0' 2&> /dev/null & disown
```

Note that this proxy is not meant to be run in the background, therefore we redirect it to
`/dev/null` and disown the process. Shutting it down would require killing its Docker container.

Then, generate a token for the predefined administrator user:

```sh
sudo kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep admin-user | awk '{print $1}')
```

Then, from your local machine, open the
[Dashboard](http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy).
Use the output token in the Dashboard login form.

**Note**: it might take some time for the Dashboard to start and for the nodes to report as ready.


## Nodes (192.168.4.11 and 192.168.4.12)

Start one or both of the node VMs.

```sh
cd node-1
vagrant up
```

Then, go to the master, and execute:

```sh
sudo kubeadm token create --print-join-command
```

Take that command and execute it on the nodes using `sudo`.


## External Services (192.168.4.20)

The external services VM is present for manual configuration of services deployed outside of the
cluster (e.g. databases).
