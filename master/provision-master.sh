#!/usr/bin/env bash
set -e

echo "Initializing master..."
sudo kubeadm init --pod-network-cidr=192.168.4.0/24 --apiserver-advertise-address=192.168.4.10

sudo -H -u vagrant bash <<EOF
  echo "Preparing home directory..."
  mkdir -p \$HOME/.kube
  sudo cp /etc/kubernetes/admin.conf \$HOME/.kube/config
  sudo chown \$(id -u):\$(id -g) \$HOME/.kube/config
EOF

echo "Configuring network..."
sudo -H -u vagrant bash <<EOF
  sudo kubectl apply -f https://docs.projectcalico.org/v3.8/manifests/calico.yaml
EOF

echo "Configuring dashboard and service accounts..."
sudo -H -u vagrant bash <<EOF
  sudo kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.0.0-beta1/aio/deploy/recommended.yaml
  sudo kubectl apply -f /vagrant/dashboard-adminuser.yml
  sudo kubectl apply -f /vagrant/default-pod-role.yml
EOF

echo "Installing the (third-party) HAProxy Ingress Controller"
sudo -H -u vagrant bash <<EOF
  kubectl create -f https://raw.githubusercontent.com/jcmoraisjr/haproxy-ingress/master/docs/haproxy-ingress.yaml
  kubectl label node idm-kube-1 role=ingress-controller
EOF
